from daemoncommons.daemon import DaemonRunner

from commanddaemon.daemon import CommandDaemon

if __name__ == '__main__':
    d = DaemonRunner('COMMANDD_CFG', ['commandd.ini', '/etc/carpi/commandd.ini'])
    d.run(CommandDaemon())
