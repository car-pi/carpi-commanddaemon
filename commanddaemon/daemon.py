from logging import Logger
from time import sleep
from typing import Optional

from carpicommons.log import logger
from daemoncommons.daemon import Daemon

from commanddaemon.commands.registry import COMMAND_REQ
from commanddaemon.listener import CommandListener


class CommandDaemon(Daemon):
    def __init__(self):
        super().__init__('CommandD')
        self._log: Optional[Logger] = None
        self._bus: Optional[CommandListener] = None
        self._running = False

    def _build_bus_reader(self) -> CommandListener:
        self._log.info('Connecting to Data Source Redis instance ...')
        return CommandListener(sub_pattern=self._get_config('Command', 'CommandPattern'),
                               host=self._get_config('Redis', 'Host', '127.0.0.1'),
                               port=self._get_config_int('Redis', 'Port', 6379),
                               db=self._get_config_int('Redis', 'DB', 0),
                               password=self._get_config('Redis', 'Password', None))

    def startup(self):
        self._log = log = logger(self.name)
        log.info('Starting up Command Daemon ...')

        self._bus = bus = self._build_bus_reader()
        self._register_commands(bus)
        bus.start()

        self._running = True

        log.info('Startup complete! Entering main loop.')
        while self._running:
            sleep(1)

    def shutdown(self):
        if self._log:
            self._log.info('Shutting down Command Daemon ...')
        if self._bus:
            self._bus.stop()
            self._bus.join()
        super().shutdown()

    def _register_commands(self,
                           bus: CommandListener):
        log = self._log
        log.info('Registering commands ...')

        for command_name, command_req in COMMAND_REQ.items():
            log.info('Registering command %s (%s)', command_name, command_req)
            bus.register_channel_callback(command_name, command_req)
