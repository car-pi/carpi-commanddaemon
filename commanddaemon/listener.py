import json
from time import sleep
from typing import Optional, Any, Callable

from redis.client import PubSub
from redisdatabus.bus import BusListener

from commanddaemon.commands.keys import get_full_command_channel


class CommandListener(BusListener):
    def __init__(self,
                 sub_pattern: str,
                 name: str = None,
                 host: str = '127.0.0.1',
                 port: int = 6379,
                 db: int = 0,
                 password: Optional[str] = None):
        super().__init__([],
                         name,
                         None,
                         host,
                         port,
                         db,
                         password)
        self._sub_pattern = sub_pattern

    def _init_listener(self) -> PubSub:
        sub = self._r.pubsub()
        sub.psubscribe(self._sub_pattern)
        return sub

    def _process_entry(self, msg: dict) -> (str, Any):
        channel_name = msg['channel'].decode('utf-8')
        param_str = msg['data'].decode('utf-8') or '{}'
        try:
            param_data = json.loads(param_str)
        except json.decoder.JSONDecodeError as e:
            self._log.error('Failed to parse parameter data from channel %s due to a JSON parsing error: %s',
                            channel_name,
                            e)
            param_data = None
        return channel_name, param_data

    def register_channel_callback(self,
                                  command: str,
                                  callback: Callable[[str, Any], Any]):
        channel = get_full_command_channel(self._sub_pattern,
                                           command)
        self._log.info('Registering callback for command %s (%s)',
                       command,
                       channel)
        super().register_channel_callback(channel, callback)

    def run(self) -> None:
        self._log.info("Starting up...")
        self._running = True

        sub = self._init_listener()
        while self._running:
            msg = sub.get_message(ignore_subscribe_messages=True,
                                  timeout=1)
            if msg and msg['type'] == 'pmessage':
                channel, data = self._process_entry(msg)

                self._log.info('{}: {}'.format(channel, data))
                self._current_data[channel] = data
                for glc in self._global_callbacks:
                    glc(channel, data)
                if channel in self._callbacks:
                    for cl in self._callbacks[channel]:
                        cl(channel, data)
            sleep(0.001)
