import os.path
from logging import Logger
from typing import Optional, Any


DL_URL = 'https://gitlab.com/api/v4/projects/32233146/jobs/artifacts/master/raw/out/webdaemon-ui.tar.gz' \
         '?job=client:build'


def update_webui_service(log: Logger,
                         _: Optional[Any]):
    try:
        os.system('bash /home/pi/update-webui "{}"'.format(DL_URL))
    except Exception as e:
        log.error('Failed to execute command: %s', e)
