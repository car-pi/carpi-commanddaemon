from logging import Logger

from carpicommons.log import logger

import commanddaemon.commands.system as sys
import commanddaemon.commands.update as update
import commanddaemon.commands.update_webui as update_webui
from commanddaemon.commands.base import CommandRequestDict


def _get_command_logger(name: str) -> Logger:
    return logger(name)


COMMAND_REQ: CommandRequestDict = {
    'update.service': lambda c, d: update.update_service(_get_command_logger(c), d),
    'update.webui': lambda c, d: update_webui.update_webui_service(_get_command_logger(c), d),
    'sys.test': lambda c, d: sys.test_command(_get_command_logger(c), d),
    'sys.shutdown': lambda c, d: sys.shutdown(_get_command_logger(c), d),
    'sys.reboot': lambda c, d: sys.reboot(_get_command_logger(c), d),
    'sys.start_service': lambda c, d: sys.start_carpi_service(_get_command_logger(c), d),
    'sys.stop_service': lambda c, d: sys.stop_carpi_service(_get_command_logger(c), d),
    'sys.restart_service': lambda c, d: sys.restart_carpi_service(_get_command_logger(c), d),
}
