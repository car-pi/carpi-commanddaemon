import os.path
from re import compile
from logging import Logger
from typing import Optional, Any


CARPI_SERVICE_RE = compile('[a-z]*')


def update_service(log: Logger,
                   params: Optional[Any]):
    if params is None or 'service' not in params:
        return
    service = params['service']
    if not CARPI_SERVICE_RE.match(service):
        log.error('Service update request rejected because service name did not match expected format (was %s)',
                  service)
        return
    service_path = '/opt/carpi/{}'.format(service)
    if not os.path.exists('/opt/carpi/{}'.format(service)):
        log.error('Cannot update service %s, it does not exist', service)
        return

    try:
        os.system('git -C "{}" pull'.format(service_path))
    except Exception as e:
        log.error('Failed to execute command: %s', e)
