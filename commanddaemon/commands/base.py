from logging import Logger
from typing import Callable, Optional, Any, Dict

Command = Callable[[Logger, Optional[Any]], None]
CommandRequestCallback = Callable[[str, Any], None]
CommandDict = Dict[str, Command]
CommandRequestDict = Dict[str, CommandRequestCallback]
