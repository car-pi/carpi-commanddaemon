import os
import re
from logging import Logger
from typing import Optional, Any


CARPI_SERVICE_RE = re.compile('carpi-([a-z]*)')


def test_command(log: Logger,
                 _: Optional[Any]):
    log.info('Got test command')


def shutdown(log: Logger,
             params: Optional[Any]):
    log.info('Shutdown requested')
    try:
        os.system('sudo poweroff')
    except Exception as e:
        log.error('Failed to execute command: %s', e)


def reboot(log: Logger,
           params: Optional[Any]):
    log.info('Reboot requested')
    try:
        os.system('sudo reboot')
    except Exception as e:
        log.error('Failed to execute command: %s', e)


def _execute_service_command(log: Logger,
                             params: Optional[Any]) -> Optional[str]:
    if params is None or 'service' not in params:
        return
    log.info('CarPi Service Command requested')
    service_name = 'carpi-{}'.format(params['service']).lower()
    if not CARPI_SERVICE_RE.match(service_name):
        log.error('Service restart request rejected because service name did not match expected format (was %s)',
                  service_name)
        return None

    if not os.path.exists('/etc/systemd/system/{}.service'.format(service_name)):
        log.error('Service %s cannot be restarted because it is not known', service_name)
        return None

    return service_name


def _restart_service(log: Logger,
                     command: str,
                     service_name: str):
    try:
        log.info('Running CarPi Service command: %s %s', command, service_name)
        os.system('sudo systemctl {} {}.service'.format(command, service_name))
    except Exception as e:
        log.error('Failed to execute command: %s', e)


def start_carpi_service(log: Logger,
                        params: Optional[Any]):
    service_name = _execute_service_command(log, params)
    if service_name:
        _restart_service(log, 'start', service_name)


def stop_carpi_service(log: Logger,
                       params: Optional[Any]):
    service_name = _execute_service_command(log, params)
    if service_name:
        _restart_service(log, 'stop', service_name)


def restart_carpi_service(log: Logger,
                          params: Optional[Any]):
    service_name = _execute_service_command(log, params)
    if service_name:
        _restart_service(log, 'restart', service_name)
